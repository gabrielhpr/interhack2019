import React, { Component } from 'react';
import Mainpage from './containers/Mainpage';
import './App.css';
import { BrowserRouter } from 'react-router-dom';


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Mainpage/>
        </div>
      </BrowserRouter>

    );
  }
}

export default App;
