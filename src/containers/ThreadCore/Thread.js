import React, { Component } from 'react';
import "./Thread.css";

class Thread extends Component {
    constructor(props){
        super(props);
        let {titulo, descricao, categoria, data} = props.location.state;
        console.log(props.location.state);
        this.titulo = titulo;
        this.descricao = descricao;
        this.categoria = categoria;
        this.data = data;
    }
    render() {
      return (
        <div>
            <div className="Thread">
                <div className="Content mx-auto">
                    <h2 className="Thread-title" style={{fontSize:"35px", fontWeight:"600"}}>{this.titulo}</h2>
                    <div className="Thread-inform">
                        <p>Publicado por funcionário/administrador/aluno</p>
                        <p>Data: 24/08/2019</p>
                    </div>
                    <h3 className="Thread-title">Descrição</h3>
                    <div className="Thread-description">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pretium venenatis massa a posuere. 
                        Vestibulum at elit viverra, egestas est posuere, auctor dui. Sed convallis, dui id consectetur pharetra, 
                        ligula nunc volutpat ex, at mattis erat dui quis urna. Curabitur ante turpis, suscipit in cursus id, aliquet 
                        at mauris. Pellentesque pulvinar quam mauris, sed mollis urna dictum non. Cras egestas mi at ex dignissim 
                        placerat. Aenean ut convallis nisl. Proin erat nibh, cursus et urna vel, mollis iaculis nibh. Cras dignissim
                        elementum tincidunt. Morbi fringilla, leo sed ultrices congue, neque ante lacinia velit, sit amet varius nunc
                        justo nec ligula. Nulla facilisi.</p>
                        <p>Donec tempor, eros vel viverra feugiat, lorem enim pharetra turpis, ac euismod mi ipsum vel nisl. 
                        Suspendisse hendrerit ipsum justo. Aenean bibendum imperdiet quam. In hac habitasse platea dictumst. 
                        Aenean sit amet sem urna. Aliquam magna diam, fringilla vitae vestibulum in, suscipit sit amet urna. 
                        Cras turpis urna, iaculis non diam vitae, consectetur aliquet magna. Phasellus finibus sem et mauris 
                        pulvinar elementum. Vivamus sagittis suscipit pulvinar. Suspendisse interdum ornare ligula, sit amet 
                        Wfringilla dolor.</p>
                        <p>Vivamus ultrices sem lacus, eget dapibus eros tempor quis. Integer vitae molestie diam. Quisque ultricies
                        , nisi a laoreet vehicula, felis felis faucibus mauris, sit amet porta ligula arcu eu tellus. 
                        Aenean bibendum eros vel erat pellentesque, sit amet tincidunt magna dapibus. Ut feugiat metus urna, 
                        eu bibendum elit fermentum eu. Integer accumsan nec sapien et sodales. Nullam nec tempus tellus, 
                        in lacinia mauris. Sed consequat purus in gravida sodales. Duis efficitur sapien tellus, eu molestie 
                       
                        </p>
                    </div>
                    <div className="Thread-reactions">
                        <button className="button-design btn btn-success col-4">Up vote</button>
                        <button className="btn btn-danger col-4">Down vote</button>
                    </div>
                </div>
            </div>
        </div>
      )
    }
  }
  
  export default Thread;