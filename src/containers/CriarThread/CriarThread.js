import React, { Component } from 'react';


class CriarThreads extends Component{



  onChangeHandler(event){
    console.log(event.target.name);
    let id = event.target.id;
    const updatedControls = { 
        ...this.state.controls, 
    };
    //Checa se todos os inputs estao validados e armazena em formIsValid
    let formValid = true;
    for(let formData in updatedControls){
        formValid = updatedControls[formData].valid && formValid;

    }
    updatedControls[id].value = event.target.value;
    updatedControls[id].valid = this.checkValidity( updatedControls[id].value, updatedControls[id].validation);

    this.setState({controls: updatedControls, formIsValid: formValid});
    console.log(this.state);
  }

  render(){
    return(
      <div>
        <div className="ml-auto">
         <form action="" onSubmit={this.submitHandler} className="col-8 mx-auto">
             <h2>Cadastro</h2>
             <h5>Propriedade/servicos</h5>

             <div className="row justify-content-center align-items-center mx-1">
                 <label className="col-2 mb-0" for="email">EMAIL</label>
                 <input className="col-6" type="email" id="email" name="emailInput" placeholder="Digite o seu email" onChange={this.onChangeHandler}/>
             </div>

             <div className="row justify-content-center align-items-center mx-1">
                 <label className="col-2 mb-0" for="password">Password</label>
                 <input className="col-6" type="password" id="password" onChange={this.onChangeHandler}/>
             </div>

             <div className="row justify-content-center align-items-center mx-1">
                 <label className="col-2 mb-0" for="password">Password</label>
                 <input className="col-6" type="password" id="password" onChange={this.onChangeHandler}/>
             </div>

             <div className="row justify-content-center align-items-center mx-1">
                 <label className="col-2 mb-0" for="password">Password</label>
                 <input className="col-6" type="password" id="password" onChange={this.onChangeHandler}/>
             </div>
            
            
             <button className="btn btn-dark mx-auto">Enviar</button>
         </form>

        </div>
      </div>
     );
  }


}
export default CriarThreads;
