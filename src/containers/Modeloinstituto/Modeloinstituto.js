import React, { Component } from 'react';
import { Route, Link, Switch } from 'react-router-dom';
import './ModeloInstituto.css';
import firebase from 'firebase/app';
import 'firebase/database';
import { configdb } from '../../dbconfig/dbconfig';

class ModeloInstituto extends Component {
	constructor(props){
		super(props);
		this.state = {
      hide1: true,
      threads:[
        
      ],
		};

		if (firebase.apps.length == 0) {
			firebase.initializeApp(configdb);
		}
	}

	componentDidMount(){

    firebase.database().ref().child('post').on('value', snap => {
			snap.forEach((snapChild) => {
				let data = snapChild.val();
				this.setState(state => {
						console.log(data.images);
						return { threads:[...state.threads, 
							{
								titulo:data.titulo, 
								data:data.data,
								categoria:data.categoria,
								descricao:data.descricao,
								local:data.local,
								quantDownVotes:data.quantDownVotes,
								quantUpVotes:data.quantUpVotes
							}]}
				});                       
			});
		});
	}

    render() {
      return (
      	<div>
         	<div className="container-fluid pb-4" style={{backgroundColor:"#d3d3d6", height: "100%"}}>
					
						<div className="pt-4">
							<div className="" style={{fontFamily:"Bree Serif", fontSize: "50px"}}>
								<h1 className="Titulo-instituto">IME</h1>
							</div>
						</div>
						
						<div className="pt-4 pb-5">
							<div className=" row mx-auto  p-3 align-items-center justify-content-center bg-white" style={{width:"58%", borderRadius:"20px"}}>
								<h4 className="col-8 mb-0">Possui alguma reclamação?</h4>
								<Link className="btn btn-info col-3" style={{fontSize: "15px", fontWeight:"500"}} to="/instituto/criarThread">CRIAR THREAD</Link>
							</div>
						</div>
						

						<div className="px-3 pb-2 mx-auto" style={{width:"80%"}} >
							<button className="col-6 btn but-modelo py-2" style={this.state.hide1 ? {backgroundColor:"gray", color:"white"}:{} } onClick={() => {this.changingVisualization('threads')} }>THREADS</button>
							<button className="col-6 btn but-modelo py-2" style={!this.state.hide1 ? {backgroundColor:"gray", color:"white"}:{} } onClick={() => {this.changingVisualization('hist')} }>HISTÓRICO</button>
						</div>

						<div className="blocoDeThreads mx-auto px-3 pb-3" hidden={!this.state.hide1} style={{width:"80%"}}>
							{this.tipoDeListaHandler()}
						</div>

						<div className="mx-auto px-3 pb-3" hidden={this.state.hide1} style={{width:"80%", height:"100%"}}>
								<div className="card card-geral my-2 p-3 bg-success" style={{borderRadius: "30px", backgroundColor:"gray"}}> 
									<div className="main-content">
										<div className="">
											<div>
												<h2 className="Titulo-thread pl-3" >Reforma do toldo que conecta o bloco B ao C</h2>
												<p className="">O toldo que apresenta problemas em sua estruturação foi reformado.</p>
											</div>
										</div>
										<hr className=""/>
										<div className="row card-info agoravai">
											<div className="col-8 mr-auto pl-4">
												<p className="ml-auto" style={{fontSize:"15px"}}>CATEGORIA: Alunos/Funcionários</p>
												<p className="ml-auto" style={{fontSize:"15px"}}>DATA: 01/08/2019</p>							
											</div>
											<div className="my-auto agoravai">
												<div>
													<p className="d-inline-block num-votacao mb-0"></p>
												</div>
												<div>
													<p className="d-inline-block num-votacao mb-0"></p>
												</div>
											</div>
										</div>
										</div>
								</div>
								<div className="card card-geral my-2 p-3 bg-success" style={{borderRadius: "30px", backgroundColor:"gray"}}> 
									<div className="main-content">
										<div className="">
											<div>
												<h2 className="Titulo-thread pl-3" >Reestabelecimento do contrato de limpeza do instituto.</h2>
												<p className="">O contrato de limpeza foi reestabelecido, possíveis transtornos foram contornados.</p>
											</div>
										</div>
										<hr className=""/>
										<div className="row card-info agoravai">
											<div className="col-8 mr-auto pl-4">
												<p className="ml-auto" style={{fontSize:"15px"}}>CATEGORIA: Alunos/Funcionários</p>
												<p className="ml-auto" style={{fontSize:"15px"}}>DATA: 01/08/2019</p>							
											</div>
											<div className="my-auto agoravai">
												<div>
													<p className="d-inline-block num-votacao mb-0"></p>
												</div>
												<div>
													<p className="d-inline-block num-votacao mb-0"></p>
												</div>
											</div>
										</div>
										</div>
								</div>
						</div>

					
         	</div>
      	</div> 
      )
	}
	changingVisualization = (whatToChange) =>{
		if(whatToChange == 'threads'){
			this.setState( (state) => ({ hide1: true, hide2: false }) );
		}
		else{
			this.setState( (state) => ({ hide1: false, hide2: true }) );
		}
  }
	
	incrementa = (tipo, i) => {
		this.setState((state) => {
			let v = [...state.threads];
			if (tipo == "upVotes") {
				let number = v[i].quantUpVotes;
				number++;
				v[i].quantUpVotes = number;
			}
			else {
				let number = v[i].quantDownVotes;
				number++;
				v[i].quantDownVotes = number;
			}
			
			//state.threads[i].quantUpVotes += 1;
			return {...state, v};
		})
	}

	

  tipoDeListaHandler = () => {
    return (
        this.state.threads.map((resp, i) => {
				if (resp.local == "IME") {
						return(
							<div className="card card-geral bg-white my-2 p-3" style={{borderRadius: "30px", backgroundColor:"gray"}}> 
							<div className="main-content ">
								<div className="">
									<div onClick={() => {this.props.history.push('/instituto/thread', this.state.threads[i])} }>
										<h2 className="Titulo-thread pl-3" >{resp.titulo}</h2>
										<p className="">{resp.descricao}</p>
									</div>
								</div>
								<hr className=""/>
								<div className="row card-info agoravai">
									<div className="col-8">
										<p className="" style={{fontSize:"15px"}}>CATEGORIA: {resp.categoria}</p>
										<p className="" style={{fontSize:"15px"}}>DATA: {resp.data}</p>							
									</div>
									<div className="my-auto agoravai">
										<div>
											<p className="d-inline-block num-votacao mb-0">{resp.quantUpVotes}</p>
											<button className="btn btn-success mx-2 but-votacao" onClick={() => this.incrementa("upVotes", i) }>UPVOTE</button>
										</div>
										<div>
											<button className="btn btn-danger but-votacao" onClick={() => this.incrementa("downVotes", i) }>DOWNVOTE</button>
											<p className="d-inline-block num-votacao mb-0">{resp.quantDownVotes}</p>
										</div>
									</div>
								</div>
								</div>
						</div>

						) 
					}                         
        })
    )
  }

}
  
  export default ModeloInstituto;