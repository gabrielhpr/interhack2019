import React, { Component } from 'react';
import Modeloinstituto from './Modeloinstituto/Modeloinstituto';
import Home from './Home/Home';
import CriarThread from './CriarThread/CriarThread';

import Thread from './ThreadCore/Thread';
import { Route, Link, Switch } from 'react-router-dom';

import "./Mainpage.css";
import logo from "../logo.png";

class Mainpage extends Component {
	render() {
		return (
			<div>
				<header>
					<nav className="navbar navbar-expand-sm text-center">
						<div className="dropdown show">
							<a className="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Instância
							</a>
							<div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
								<a className="dropdown-item" href="/campus">USP Butantã</a>
								<a className="dropdown-item" href="/instituto">IME</a>
							</div>
						</div>
						<div className="logo">
							<Link className="nav-link text-dark" to="/" ><img src={logo} alt="Connect"/></Link>
						</div>
						
						<div className="collapse navbar-collapse">
							<ul className="navbar-nav ml-auto align-items-center" >
								<li className="nav-item"><Link className="nav-link p-1 px-2 rounded mr-2" to="/" >LOGIN</Link></li>
							</ul>
						</div>
					</nav>
				</header>
				<Switch>
					<Route path="/:id/criarThread" exact component={CriarThread} />
					<Route path="/:id/thread" component={Thread} />
					<Route path="/instituto" exact component={Modeloinstituto} />
					<Route path="/campus" exact component={Home} />
					<Route path="/" exact component={Home} />
				</Switch>
			</div>
		)
	}
}

export default Mainpage;
