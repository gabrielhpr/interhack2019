import React, { Component } from 'react';
import { Route, Link, Switch } from 'react-router-dom';
import './Home.css';
import firebase from 'firebase/app';
import 'firebase/database';
import { configdb } from '../../dbconfig/dbconfig';

class Home extends Component {
	constructor(props){
		super(props);
		this.state = {
      hide1: true,
      threads:[
        
      ],
		};

		if (firebase.apps.length == 0) {
			firebase.initializeApp(configdb);
		}
	}

	componentDidMount(){

    firebase.database().ref().child('post').on('value', snap => {
			snap.forEach((snapChild) => {
				let data = snapChild.val();
				this.setState(state => {
						console.log(data.images);
						return { threads:[...state.threads, 
							{
								titulo:data.titulo, 
								data:data.data,
								categoria:data.categoria,
								descricao:data.descricao,
								local:data.local,
								quantDownVotes:data.quantDownVotes,
								quantUpVotes:data.quantUpVotes
							}]}
				});                       
			});
		});
	}

    render() {
      return (
      	<div>
         	<div className="container-fluid pb-4" style={{backgroundColor:"#d3d3d6", height: "100%"}}>
					
						<div className="pt-4">
							<div className="" style={{fontFamily:"Bree Serif", fontSize: "50px"}}>
								<h1 className="Titulo-instituto">USP Butantã</h1>
							</div>
						</div>
						
						<div className="pt-4 pb-5">
							<div className=" row mx-auto  p-3 align-items-center justify-content-center bg-white" style={{width:"58%", borderRadius:"20px"}}>
								<h4 className="col-8 mb-0">Possui alguma reclamação?</h4>
								<button className="btn btn-secondary col-3" style={{fontSize: "15px", fontWeight:"500"}}>CRIAR THREAD</button>
							</div>
						</div>
						

						<div className="px-3 pb-2 mx-auto" style={{width:"80%"}} >
							<button className="col-6 btn but-modelo py-2" style={this.state.hide1 ? {backgroundColor:"gray", color:"white"}:{} } onClick={() => {this.changingVisualization('threads')} }>THREADS</button>
							<button className="col-6 btn but-modelo py-2" style={!this.state.hide1 ? {backgroundColor:"gray", color:"white"}:{} } onClick={() => {this.changingVisualization('hist')} }>HISTÓRICO</button>
						</div>

						<div className="blocoDeThreads mx-auto px-3 pb-3" hidden={!this.state.hide1} style={{width:"80%"}}>
							{this.tipoDeListaHandler()}
						</div>

					
         	</div>
      	</div> 
      )
	}
	changingVisualization = (whatToChange) =>{
		if(whatToChange == 'threads'){
			this.setState( (state) => ({ hide1: true, hide2: false }) );
		}
		else{
			this.setState( (state) => ({ hide1: false, hide2: true }) );
		}
  }
	
	incrementa = (tipo, i) => {
		this.setState((state) => {
			let v = [...state.threads];
			if (tipo == "upVotes") {
				let number = v[i].quantUpVotes;
				number++;
				v[i].quantUpVotes = number;
			}
			else {
				let number = v[i].quantDownVotes;
				number++;
				v[i].quantDownVotes = number;
			}
			
			//state.threads[i].quantUpVotes += 1;
			return {...state, v};
		})
	}

	

  tipoDeListaHandler = () => {
    //this.ordenaListaPorPreco(tipoLocalOuServico,especificacao, local);
    return (
        this.state.threads.map((resp, i) => {
           if (resp.local == "USP Butantã") {
						return(
							<div className="card card-geral bg-white my-3" style={{borderRadius: "10px", backgroundColor:"gray"}}> 
									<div onClick={() => {this.props.history.push('/campus/thread', this.state.threads[i])} }>
								<h3 className="card-title mt-4 pl-5 text-left">{resp.titulo}</h3>
								<p className="text-left pl-5 mb-1">{resp.descricao}</p>
							</div>
							<hr className="mb-0"/>
							<div className="row">
								<div className="col-6">
									<p className="text-left pl-5 mt-3" style={{fontSize:"15px"}}>CATEGORIA: {resp.categoria}</p>
									<p className="text-left pl-5" style={{fontSize:"15px"}}>DATA: {resp.data}</p>							
								</div>
								<div className="my-auto col-6">
									<div className="d-inline">
										<p className="d-inline-block num-votacao mb-0">{resp.quantUpVotes}</p>
										<button className="btn btn-success mx-2 d-inline-block but-votacao" onClick={() => this.incrementa("upVotes", i) }>UPVOTE</button>
									</div>
									<div className="d-inline">
										<button className="btn btn-danger mx-2 d-inline-block but-votacao" onClick={() => this.incrementa("downVotes", i) }>DOWNVOTE</button>
										<p className="d-inline-block num-votacao mb-0">{resp.quantDownVotes}</p>
									</div>
								</div>
							</div>
						</div>

						)
					 } 
        })
    )
  }

}
  
  export default Home;